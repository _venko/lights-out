use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

use priority_queue::PriorityQueue;

use std::collections::HashSet;
use std::fmt;
use std::fmt::Display;

const DEBUG: bool = false;
const SIZE: usize = 5;
const MOVE_WEIGHT: isize = 1;
const USE_SIMPLE_SCORING: bool = false;

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
enum Light {
    Off,
    On,
}

impl Default for Light {
    fn default() -> Light {
        Light::Off
    }
}

impl Display for Light {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Light::Off => write!(f, "⬛"),
            Light::On => write!(f, "🟦"),
        }
    }
}

impl Distribution<Light> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Light {
        match rng.gen::<bool>() {
            false => Light::Off,
            true => Light::On,
        }
    }
}

impl Light {
    fn toggle(&self) -> Light {
        match self {
            Light::Off => Light::On,
            Light::On => Light::Off,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
struct Board([[Light; SIZE]; SIZE]);

impl Default for Board {
    fn default() -> Board {
        let board = [[Light::default(); SIZE]; SIZE];
        Board(board)
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let board = self.0;
        for row in board {
            for col in row {
                write!(f, "{}", col)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Distribution<Board> for Standard {
    fn sample<R: Rng + ?Sized>(&self, _: &mut R) -> Board {
        let mut board = [[Light::default(); SIZE]; SIZE];
        for i in 0..SIZE {
            for j in 0..SIZE {
                board[i][j] = rand::random();
            }
        }
        Board(board)
    }
}

#[allow(dead_code)]
impl Board {
    fn new() -> Board {
        Board::default()
    }

    fn neighbors(&self, row: usize, col: usize) -> Vec<(usize, usize)> {
        let row = row as isize;
        let col = col as isize;
        vec![
            (row - 1, col),
            (row + 1, col),
            (row, col - 1),
            (row, col + 1),
        ]
        .iter()
        .filter(|(row, col)| 0 <= *row && *row < SIZE as isize && 0 <= *col && *col < SIZE as isize)
        .map(|(row, col)| (*row as usize, *col as usize))
        .collect()
    }

    fn make_move(&self, row: usize, col: usize) -> Board {
        let neighbors = self.neighbors(row, col);
        let mut new_board = *self;

        new_board.0[row][col] = self.0[row][col].toggle();
        for (r, c) in neighbors {
            new_board.0[r][c] = self.0[r][c].toggle();
        }

        new_board
    }
}

type Score = isize;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Game {
    board: Board,
    moves: usize,
}

impl Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.board)?;
        writeln!(f, "Moves: {}", self.moves)?;
        Ok(())
    }
}

impl Game {
    fn new(board: Board, moves: usize) -> Game {
        Game { board, moves }
    }

    fn score(&self) -> Score {
        let mut score = -MOVE_WEIGHT * self.moves as isize;

        if USE_SIMPLE_SCORING {
            return score;
        }

        // TODO: Implement IntoIterator for boards.
        for row in self.board.0 {
            for cell in row {
                if cell == Light::default() {
                    score += 1;
                }
            }
        }
        score
    }

    fn won(&self) -> bool {
        let fail = Light::default().toggle();
        for row in self.board.0 {
            for cell in row {
                if cell == fail {
                    return false;
                }
            }
        }
        true
    }

    fn successors(&self) -> Vec<Game> {
        let mut succs: Vec<Game> = Vec::with_capacity(25);
        for row in 0..SIZE {
            for col in 0..SIZE {
                succs.push(Game::new(self.board.make_move(row, col), self.moves + 1));
            }
        }
        succs
    }
}

fn main() {
    use Light::*;
    let board = Board([
        [On, On,  On,  On,  On],
        [On, On,  Off, On,  On],
        [On, Off, Off, Off, On],
        [On, On,  Off, On,  On],
        [On, On,  On,  On,  On],
    ]);

    println!("{}", board);

    let game = Game::new(board, 0);
    let mut iters = 0;
    let mut frontier: PriorityQueue<Game, Score> = PriorityQueue::new();
    let mut seen = HashSet::new();
    frontier.push(game, game.score());

    while let Some((current, _)) = frontier.pop() {
        seen.insert(current.board);

        if DEBUG {
            println!(
                "Iteration: {} | Seen: {} | Frontier: {}\n{}\n",
                iters,
                seen.len(),
                frontier.len(),
                current
            );
        }

        if current.won() {
            println!("{}", current);
            break;
        }

        for next in current.successors() {
            if !seen.contains(&next.board) {
                frontier.push(next, next.score());
            }
        }
        iters += 1;
    }
    println!("Completed in {} iterations.", iters);
}
